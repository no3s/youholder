import random
import subprocess
from PIL import Image, ImageDraw, ImageFont
from pathlib import Path
import numpy as np
import gleitzsch
import os
import cv2



def drawTextWithOutline(draw, font, text, x, y):
    bc = 'rgb(0, 0, 0)' # black color
    wc = 'rgb(255,255,255)' # black color
    draw.text((x-2, y-2), text,bc,font=font)
    draw.text((x+2, y-2), text,bc,font=font)
    draw.text((x+2, y+2), text,bc,font=font)
    draw.text((x-2, y+2), text,bc,font=font)
    draw.text((x, y), text, wc, font=font)
    return


def images_to_video(filenames, output, fps, info, params, font, image):
    '''
    builds a video from a list of images
    IN: list of filenames for images, where to output, , frames per second
    OUT: filename for output video
    '''
    from cv2 import VideoWriter, VideoWriter_fourcc, imread, destroyAllWindows

    print("CREATING VIDEO", output, fps, info, params)
    fourcc = VideoWriter_fourcc(*'mp4v')
    vid = None
    
    image2 = None
    tname = params["temp_dir"] + "black_temp.png"
    
    if not image:       
        image2 = Image.new('RGB', (1920,1080), 'rgb(0, 0, 0)')
        image2.save(tname)    
    else:        
        image2 = Image.open(image).resize((1920,1080), Image.ANTIALIAS).convert('RGB')        
        image2.save(tname)   
        
    max_n = params['max_render_fps'] 
    change_scene = random.randrange(50,400)
    counter = 0
    for n,file in enumerate(filenames): 
                                
        if n > max_n:
            break
            
        image = Image.open(file).convert("RGBA")   
        x = np.asarray(image).copy()
        x[:, :, 3] = (0 + x[:, :, :3].mean(axis=2)).astype(np.uint8)
        image = Image.fromarray(x)
                
                
        if n == 0 or (counter % change_scene == 0):
            random.seed()
            change_scene = random.randrange(25,400)            
            counter = 0
            cmd =  "python3 " + os.getcwd() + "/gleitzsch.py "+ params['glitz_opts']+" --size 1920 --temp_dir " +  params["temp_dir"] + " " + tname +" "+ file[:-4]+"-glei8.jpg" 
            print("CALLING\t{}".format(cmd))
            print("Changed scene: " , n, counter, change_scene, file)
            try:
                subprocess.call(cmd, shell=True)                   
                image2 = Image.open(file[:-4]+"-glei8.jpg").convert("RGBA") 
                print("Glitched " + str(n))
            except :
                print("some error")
              
        image = Image.alpha_composite(image2, image)            
        draw = ImageDraw.Draw(image)    
                    
        drawTextWithOutline(draw, font, info["artist"], 20, image.height - 130)
        drawTextWithOutline(draw, font, info["song"], 20, image.height - 100)
        drawTextWithOutline(draw, font, info["album"], 20, image.height - 70)
        
        image = cv2.cvtColor(np.array(image.convert("RGB")), cv2.COLOR_RGB2BGR)

        # now that we have an image we can setup vid
        if vid is None:
            size = image.shape[1], image.shape[0]
            print("SIZE:\t{}".format(size))
            # false for is_color
            vid = VideoWriter(output, fourcc, float(fps), size)

        vid.write(image)
        counter = counter + 1
        
    #vid.release()
    destroyAllWindows()
    return output


def add_audio(video, audio, outfile):
    '''
    combines audio and visual channels into new .mp4
    IN: video filename, audio filename
    OUT: saves a video to auio_+video_filename
    WARNING: this is where code is bad, you need ffmpeg installed for it to work
    '''    
    
    cmd = "rm -f {}".format(outfile)
    print("CALLING\t{}".format(cmd))
    subprocess.call(cmd, shell=True)
    
    print("ADDING AUDIO")
    cmd = "ffmpeg -i {} -i {} -vcodec copy {}".format(video, audio, outfile)
    print("CALLING\t{}".format(cmd))
    subprocess.call(cmd, shell=True)
    
    cmd = "rm -f {}".format(video)
    print("CALLING\t{}".format(cmd))
    subprocess.call(cmd, shell=True)

    
# duration specifies the number of seconds between frames
# lowest supported value is 0.1 I think
def create_gif(filenames, output, duration):
    '''
    IN: filenames to build gif from, where to save gif, time between frames
    OUT: saves a .gif to the requested output
    '''
    import imageio
    print("BUILDING GIF")
    images = []
    for file in filenames:
        images.append(imageio.imread(file))
    imageio.mimsave(output, images, duration=duration)


def video_from_folder(folder, output, fps):
    '''
    builds a video from all the .pngs in a folder
    IN: folder, output location, frames per second
    OUT: save location
    '''
    print("GETTING .PNG FROM:\t{}".format(folder))
    import os
    filenames = os.listdir(folder)

    for fichier in filenames[:]:  # filelist[:] makes a copy of filelist.
        if not(fichier.endswith(".png")):
            filenames.remove(fichier)

    from cv2 import VideoWriter, VideoWriter_fourcc, imread, destroyAllWindows

    print("CREATING VIDEO")
    fourcc = VideoWriter_fourcc(*'mp4v')
    vid = None
    for file in filenames:
        img = imread("{}/{}".format(folder, file))
        # now that we have an image we can setup vid
        if vid is None:
            size = img.shape[1], img.shape[0]
            print("SIZE:\t{}".format(size))
            # false for is_color
            vid = VideoWriter(output, fourcc, float(fps), size)

        vid.write(img)

    vid.release()
    destroyAllWindows()
    return output

# output = video_from_folder('covers', 'ultralight.mp4', 30)
# add_audio("ultralight.mp4", "ultralight.wav")
