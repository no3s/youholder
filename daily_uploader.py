import sys
import os
import subprocess
import argparse
import time

def daily_uploads(finput, output, numlines):
    
    fp2 = open(output, 'w')
    fp = open(finput, 'r')
        
    x = 0
    lines = fp.readlines()
    fp.close()
    
    for x in range(0,numlines):
        line = lines.pop(0)
        print(line)           
        subprocess.call(line, shell=True)
        print(len(lines))
        fp = open(finput, 'w')
        for l in lines:
            fp.write(l)
        fp.close()
        fp2.write("Processing:\n " + line)
        time.sleep(2)
        
    fp2.close()
        
def main():
    parser = argparse.ArgumentParser(description="Check uploads file and upload video to youtube")
    parser.add_argument('-i','--input_file',  required=True)
    parser.add_argument('-o','--output_log_file',  required=True)
    parser.add_argument('-l', '--parse_num_lines', default=4, type=int)
          
    args = parser.parse_args()
    print(args)                
    daily_uploads(args.input_file, args.output_log_file, args.parse_num_lines)

    
if __name__ == "__main__":    
    main()
    
    