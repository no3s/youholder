import waveform 
import mutagen
import argparse
import time
from images_to_video import images_to_video, add_audio
from PIL import ImageFont
from pathlib import Path
import random 

def create_joyfreq_imageset(file, output, fps, font_ttf, dir_temp, params):
    params['save_loc'] = dir_temp + "uframe.png"
    params['temp_wav'] = dir_temp + "uwav.wav"
    params['temp_dir'] = dir_temp
    
    print(params)
    
    rate, amps = waveform.get_amplitudes(file, params)
    frame_rate = int(fps)

    # both are ints so using // should return an int
    rate_div_fps = rate//frame_rate

    # the number of items (frames) in iteration_list is determined by
    # items < len(amps)/rate_div_fps or len(amps)//rate_div_fps
    n_frames = len(amps)//rate_div_fps
    iteration_list = [i*rate_div_fps for i in range(n_frames)]

    print("BUILDING {} IMAGES".format(n_frames))

    # precalculate some things for build_album_cover
    n_data_needed = params['data_line']*params['n_lines']
    im_width = (params['spacers']+1)*(2*(params['noise_frac']*params['data_line'])+params['data_line'])*2


    im_width = 1920

    im_height = 1080
    #int(im_width/0.85)

    # precalculate the indexes and x cords for drawing for add_wave
    indexes = range(0, (params['data_line']*(2*params['noise_frac']+1)-1))
    x_cords = [i*(params['spacers']+1) for i in indexes]

    filenames = []
    max_n = params['max_render_fps'] 
        
    for i, loc in zip(iteration_list, range(n_frames)):
        if i % 250 == 0:
            print(i,loc, loc>max_n,n_frames, max_n, file)
        if loc > max_n:     
            print(i,loc, loc>max_n,n_frames, max_n, file)
            break
            
        filenames.append(
            waveform.build_album_cover(amps[i:i+n_data_needed], loc, im_width, im_height, indexes, x_cords, params)
        )
       
    print("BUILT:\t{}/{}".format(max_n, n_frames))

    out_file = output

    mp3info = mutagen.File(file)
    print(mp3info)
    
    info = {"artist": str(mp3info['TPE1'].text[0]),    
            "album": str(mp3info['TALB'].text[0])}
    
    if not mp3info.get('TIT2'):
        info["song"] = "No name"
    else:
        info["song"] = str(mp3info['TIT2'].text[0])
    
    print(out_file, params, info, len(filenames), frame_rate)    
    font = ImageFont.truetype(font_ttf, size=32)
    print("Using font: " + font_ttf)
    out_file = images_to_video(filenames, out_file, frame_rate, info, params, font, params['image_load'])
    
    file = "'" + file + "'"
    output_name = "'" + out_file+".audio.mp4" + "'"
    out_file = "'" + out_file + "'"

    
    add_audio(out_file,file,output_name)


def main():
    random.seed()
    parser = argparse.ArgumentParser(description="make waveform video from music, add mp3 info")
    parser.add_argument('-f','--font_ttf', help="Font ttf", default='/home/no3z/jupyter/PermanentMarker-Regular.ttf')
    parser.add_argument('-o','--video_output', help="Output file .mp4", required=True)
    parser.add_argument('-i','--audio_source', help="Audio source", required=True)
    parser.add_argument('-t','--temp', help="Temp", default = "/tmp/")
    parser.add_argument('-fps','--fps', help="fps", default = 25, type=int)
    parser.add_argument('-im','--image', required=False, default="")
    parser.add_argument('-go','--glitz_opts', help="", default = "random")
    parser.add_argument('-max_fps','--max_render_fps', help="max_render_fps", default=0, type=int)


    args = parser.parse_args()
    print(args)                
       
    params = {
        # number of spacers between each data point
        'spacers': random.randrange(12,36),
        # number of pixels added as offset per wave
        'offset': random.randrange(11,21),
        # how many amp data points per line
        'data_line': random.randrange(13,40),
        # number of lines
        'n_lines': random.randrange(20,50),
        # percentage of actual data that is added as noise at end
        'noise_frac': 1,
        # min ticks to reach mean when drawing connecting line
        'min_ticks': random.randrange(30,150),
        # how much range does the random noise have
        'random_noise_range': random.randrange(0,4),
        # noise range for connecting line
        'connecting_line_range': random.randrange(3,8),
        # frames per second
        'fps': 30,
        # thickness of waveform lines
        'line_width': random.randrange(3,16),
        # value to scale amplitudes to
        'scale_val': 118,
    }
        
    if args.max_render_fps == 0:
        params['max_render_fps'] = int(99999)
    else:
        params['max_render_fps'] = int(args.max_render_fps)
        
    params['image_load'] = args.image
        
    if args.glitz_opts != 'random':
        params['glitz_opts'] = args.glitz_opts
    else:

        back = random.choice(['-s', '-f', '-f -s'])
        br = str("--bitrate " + str(random.randrange(2,16)) + "")
        bitrate = random.choice([br, ""])
        amplify = random.choice(["-a",""])
        rainbow = random.choice(["-r",""])
        glitter = random.choice(["-g",""])
        streaks = random.choice(["-v",""])
        hs_streaks = random.choice(["--hs",""])
        q = "-q " + str(random.randrange(3,9))
        quality = random.choice([q,""])
        h = "--hue_shift " + str(random.uniform(-1,1))
        hue = random.choice([h,""])
        bayer = random.choice(["--bayer",""])
        ga = "--gamma " + str(random.uniform(0.3,2))
        gamma = random.choice([ga,""])
        sh = "--shift " + str(random.randrange(-190,190))
        shift = random.choice([sh,""])
        blue_red = random.choice(["-b 2",""])
        interlace  = random.choice(["-i",""])

        glitz_compose = back + " " + \
                        bitrate + " " + \
                        amplify + " " + \
                        rainbow + " " + \
                        glitter + " " + \
                        streaks + " " + \
                        hs_streaks + " " + \
                        gamma + " " + \
                        shift + " " + \
                        blue_red + " " + \
                        interlace + " " + \
                        streaks + " " + quality + " " 

        show_text = ''
        for k,v in params.items():
            show_text = show_text + k[0] + ":"+str(v)+"_"
        show_text = show_text + "_C_" + glitz_compose
        show_text.replace(' ','')
        show_text.replace('  ','')
        show_text.replace('   ','')
        show_text.replace('    ','')
        show_text.replace('     ','')
        show_text.replace('      ','')
        show_text.replace('       ','')
        stext = " --text '" + show_text + "'"
        show_text = random.choice([stext,""])
        params['glitz_opts'] = glitz_compose + show_text 
        print(glitz_compose)
              
              
    start_time = time.time()
    create_joyfreq_imageset(args.audio_source, args.video_output, args.fps, args.font_ttf, args.temp, params)

    print("EXEC TIME:\t{} seconds".format(time.time() - start_time))
    

if __name__ == "__main__":    
    main()