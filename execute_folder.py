import os, sys
import subprocess
import argparse
import mutagen

def generate_videos(dinput, doutput, image, tempdir, tags):
    
    files = []
    
    for n, file in enumerate(sorted(os.listdir(dinput))):
        if file.endswith(".mp3"):
            fi = (os.path.join(dinput, file))
            fo = (os.path.join(doutput, file[:-4]+".mp4"))
            cmd =  "python3 " + os.getcwd() + "/musicvid.py -i '"+fi+"' -o '"+fo+"' -fps 25 -t "+ tempdir
            if image:
                cmd = cmd + " -im '" + image + "'"
                
            print("CALLING\t{}".format(cmd))
            subprocess.call(cmd, shell=True)
        
            #Create youtube upload file
            mp3info = mutagen.File(fi)                
            info = {"artist": str(mp3info['TPE1'].text[0]),
                    "song": str(mp3info['TIT2'].text[0]),
                    "album": str(mp3info['TALB'].text[0])}
                        
            title = info['artist'] + " - " + info['album'] + " - " + str(n) + " - " + info['song' ]
            title = title.replace("'","")
            playlist = info['artist'] + " - " + info['album']
            playlist = playlist.replace("'","")
            input_video_name = "'" + fo + ".audio.mp4'"
            
            print("Adding: " + title)
            print("Playlist: " + playlist)
            print("video_name: " + input_video_name)
            
            of = open("/home/no3z/jupyter/youtube-upload.txt", "a+")                        
                        
            upload_line = ("/usr/local/bin/youtube-upload " + \
                "--title='{}' --description='{}' --category='Music' --tags='{}' --default-language='es' " + \
                "--default-audio-language='es' --client-secrets='/home/no3z/jupyter/youholder/client_secrets.json' " + \
                "--credentials-file='/home/no3z/jupyter/youholder/credentials.json' " + \
                "--playlist='{}' --privacy='public' --embeddable=True {}\n").format(title,title,tags,playlist,input_video_name)
            
            print(upload_line)                                                
            of.write(upload_line)
            of.close()
            
        
def main():
    parser = argparse.ArgumentParser(description="make waveform video from music, add mp3 info")
    parser.add_argument('-i','--input_dir',  required=True)
    parser.add_argument('-o','--output_dir', required=True)
    parser.add_argument('-t','--temp_dir', required=True)
    parser.add_argument('-im','--image', required=False, default="")
    parser.add_argument('--tags','--tags')
      
    args = parser.parse_args()
    print(args)                
    generate_videos(args.input_dir, args.output_dir, args.image, args.temp_dir, args.tags)

    
if __name__ == "__main__":    
    main()
    
    